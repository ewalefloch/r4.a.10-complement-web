var all_countries = {};
var all_currencies = {};
var all_languages = {};



class Country {

    constructor(data) {
        this.codeCurrencies = [];
        this.codeLanguages = [];

        this.alpha3Code = data["alpha3Code"];
        this.area = data["area"];
        this.borders = (data["borders"] || "island");
        this.capital = data["capital"];
        this.continent = data["region"];
        this.demonym = data["demonym"];
        this.flags = data["flag"];
        this.name = new Names(data["name"], data["translations"]);
        this.population = data["population"];
        this.topLevelDomain = data["topLevelDomain"];
        if(data.currencies!==undefined){
            data["currencies"].forEach(currency => {
                this.codeCurrencies.push(currency.code);
            });
        }else{
            this.codeCurrencies.push("undefined");
        }

        if(data.languages!==undefined){
            data["languages"].forEach(lang => {
                this.codeLanguages.push(lang.iso639_2);
            });
        }else{
            this.codeLanguages.push("undefined");
        }

    }

    toString() {
        return "Noms : " + this.name.eng + ", captiale : " + this.capital + ", contient : " + this.continent + " et population : " + this.population;
    }

    get getPopDensity() {
        return this.population / this.area;
    }

    getBorders() {
        if(this.borders!="island"){
            const borderCountries = {};
            this.borders.forEach(code => {
                borderCountries [code] = all_countries[code];            
            });
            return borderCountries;
        }else{
            return "island";
        }
    }

    getCurrencies() {
        const currencies = {};
        for (let code in all_currencies) {
            for (let key in this.codeCurrencies){
                if(this.codeCurrencies[key]==code){
                    currencies[code] = all_currencies[code];
                }
            }
        };
        return currencies;
    }
    getLanguages() {
        const languages = {};
        for (let iso639_2 in all_languages) {
            for (let key in this.codeLanguages){
                if(this.codeLanguages[key]==iso639_2){
                    languages[iso639_2] = all_languages[iso639_2];
                }
            }
        };
        return languages;
    }

}

class Names {
    
    constructor(name, data) {
        this.fr = data["fr"];
        this.eng = name;
        this.de = data["de"];
        this.es = data["es"];
        this.it = data["es"];
    }
}

class Currency {

    constructor(data) {
        this.code = data.code;
        this.name = data.name;
        this.symbol = data.symbol;
    }

    toString() {
        return "Code : " + this.code + ", name : " + this.name + "and data : " + this.symbol;
    }
}

class Language {

    constructor(data) {
        this.code = data.code;
        this.name = data.name;
    }

    toString() {
        return "Code : " + this.code + " and name : " + this.name
    }

}


function fill_db(contries){
    /*
    fetch('countries.json')
    .then(response => response.json())
    .then(data => {
    */
    contries.forEach(contry => {
        if(contry.currencies!=undefined){
            contry.currencies.forEach(contryValeur =>{
                if (!all_currencies.hasOwnProperty(contryValeur.code)){
                    all_currencies[contryValeur.code] = new Currency(contryValeur);
                }
            });
        
        }
        if(contry.languages!=undefined){
            contry.languages.forEach(contryValeur2 =>{
                if(!all_languages.hasOwnProperty(contryValeur2.code)){
                    all_languages[contryValeur2.iso639_2] = new Language(contryValeur2);
                }
            });
        }   

        all_countries[contry.alpha3Code] = new Country(contry);
    });


}

